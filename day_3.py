'''Play with operators 
For this challenge, you will be given the values of principal, interest and year. You need to calculate the simple interest, round it to the nearest integer and print it.

Input Format
There will be 3 lines of numeric input - 
'a' : principal which is of type double. 
'b' : interest which is of type integer. 
'c' : year which is again of type integer. 

Constraints
1 < = (a,b,c) < = 1000

Output Format
Just print the simple interest value after performing the calculation using the formula to the stdout. The result should be an integer. 
'''


def main():

    principle = int(input())
    interest = int(input())
    year = int(input())

    result = round((principle*interest*year)/100)
    print(result)

main()