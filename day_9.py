'''
For this challenge, you will take an integer input and store it in a variable and checks whether the input number is a Narcissistic number or not. If it is, then print 'True' else print 'False'.


Input Format
A single integer value to be taken as input from stdin and stored it in a variable of your choice. 

Constraints
1 < = n < = 18

Output Format
print 'True' if your number is Narcissistic otherwise print 'False' to the stdout. 
'''

def main():

    number = int(input())
    temp = number
    result = 0
    while temp > 0:
        remainder = temp % 10
        result += remainder ** len(str(number))
        temp = temp // 10
    if number == result :
        print(True)
    else:
        print(False)

main()