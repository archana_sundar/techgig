'''
Lets make a dictionary order 
You need to input N words one on each line and output should be lexicographically sorted i.e the words which comes as a output should be alphabetically sorted

Input Format
You will be taking an integer N from STDIN.
Following N lines contains string one on each line.

'''
def main():
    n = int(input())
    word_list = []
    for i in range(n):
        word_list.append(input())
    word_list = sorted(word_list)
    for i in word_list:
        print(i,end="\n")

main()