'''For this challenge, you need to read a line from stdin and check whether it is of type integer, float or string.
If input is-
    Integer print 'This input is of type Integer.' to the stdout
    Float print 'This input is of type Float.' to the stdout
    String print 'This input is of type string.' to the stdout
    else print 'This is something else.' to the stdout.

Input Format
A single line to be taken as input as save it to a variable(name of your choice). '''


def main():
    value = input()
    try:
        var = int(value) 
        print("This input is of type Integer.",end="\n")
    except ValueError:
        try:
            var = float(value) 
            print("This input is of type Float.",end="")
        except ValueError:
            try:
                var = str(value)
                print("This input is of type string.",end="")
            except ValueError:
                print("This is something else.",end="")
main()
