'''
How Much Big Is Your Number (100 Marks)
For this challenge, you will take an integer input from stdin, store it in a variable and  calculate the number of digits in the number using division operator.

Input Format
A single integer value to be taken as input from stdin and stored it in a variable of your choice. 

Constraints
1 < = n < = 18

Output Format
Print the value which you will get after calculating the number of digits

'''

def main():
    count = 0
    number = int(input())
    for i in str(number):
        count +=1
    print(count)

main()