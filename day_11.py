'''
Count the letters (100 Marks)
For this challenge, you need to take a string as an input from the stdin, count the number of uppercase and lowercase letters and print them to the stdout.

Input Format
A single line of string to be taken as an input and store it in a variable of your choice. 

Constraints
1 < = |s| < = 100000 

Output Format
print the number of uppercase letters on one line and number of lowercase letters on another side.
'''

def main():

    lower = 0
    upper = 0
    string = input()
    for i in string:
        if i.islower():
            lower+=1
        if i.isupper():
            upper += 1
    print(upper,lower,sep="\n")
main()