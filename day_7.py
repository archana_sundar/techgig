'''
Count special numbers between boundaries
For this challenge, you are given a range and you need to find how many prime numbers lying between the given range.

Input Format
For this challenge, you need to take two integers on separate lines. These numbers defines the range. 

Constraints
1 < = ( a , b ) < = 100000

Output Format
output will be the single number which tells how many prime numbers are there between given range.
'''

def main():
    lower = int(input())
    upper = int(input())
    count = 0
    for num in range(lower,upper):  
        if num > 1:  
            for i in range(2,num):  
                if (num % i) == 0:  
                    break  
            else:  
                count += 1
    print(count)
main()