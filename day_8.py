'''
Is your Number Armstrong? 
For this challenge, you need to take an integer input and store it in a variable of your choice and checks whether this number is an Armstrong number or not. If yes print 'True' else print 'False'.

Input Format
A single integer value to be taken as input from stdin and stored it in a variable. 

Constraints
1 < = n < = 18

Output Format
print 'True' if your number is Armstrong otherwise print 'False' to the stdout.

'''
def main():

    armstrong_number = int(input())
    temp = armstrong_number
    result = 0
    while temp > 0:
        remainder = temp % 10
        result += remainder ** 3 
        temp  = temp // 10
    if result == armstrong_number:
        print(True)
    else:
        print(False)

main()