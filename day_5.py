'''Loop your world 
For this challenge, you need to take an integer value as input from stdin, calculate its factorial and print the result to the stdout. 1

Input Format
A single integer value to be taken as input from stdin and stored it in a variable of your choice. 

Constraints
1 < = n < = 15

Output Format
Print the value which you will get after calculating the factorial of the input.'''

def main():

    number = int(input())
    result = 1
    while(number != 0):
        result = result * number
        number -= 1
    print(result)

main()